package cpsc441;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;
import java.io.*;

/**
 * Created by keynan on 18-Sep-14.
 */
public class SocketCoding {
    public static ExecutorService serverThread = Executors.newSingleThreadExecutor();

    public static void main(String [] args) throws Exception {

        final CyclicBarrier cb = new CyclicBarrier(2);
        final int x = 42;

        serverThread.submit(new Runnable() {
            public void run() {
                ExecutorService clientHandler = Executors.newFixedThreadPool(4);
                // bind to the default address
                try {
                    ServerSocket server = new ServerSocket(8080);
                    server.setSoTimeout((int) 1e4);
                    cb.await();
                    System.out.println(x);
                    while (true) {
                        Socket client = server.accept();
                        clientHandler.submit(new Request(client));
                    }
                } catch (BrokenBarrierException e) {
                } catch (InterruptedException e) {
                    clientHandler.shutdown();
                } catch (IOException e) {
                    clientHandler.shutdown();
                }

            }
        });

        //InetAddres.getHostByName("localhost");
        cb.await();
        Socket client = new Socket("localhost", 8080);
        try{

            new PrintStream(client.getOutputStream()).println("o, world!");
            client.shutdownOutput();

            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            System.out.println(br.readLine());

            int last = client.getInputStream().read();
            for(int i=10; i!=0 && last != -1; i--) {
                System.err.println("No EOF sent? got " + last);
                last = client.getInputStream().read();
            }
        }finally{
            client.close();
            serverThread.shutdown();
        }
    }


}

// http://blog.netherlabs.nl/articles/2009/01/18/the-ultimate-so_linger-page-or-why-is-my-tcp-not-reliable
class Request implements Runnable {

    final Socket client;

    public Request(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try {
            // by default Java blocks forever. Let's fix that.
            client.setSoTimeout((int)1e4);

            //send a heart-beat
            client.setKeepAlive(true);

			int bite;
			while((bite = client.getInputStream().read()) != -1){
				client.getOutputStream().write(bite);
			}
			
            client.shutdownOutput();
            //hang around for a while
            //  until we're sure the otherside is done talking
            //client.setSoLinger(true, 10000);

            client.shutdownInput();  

            
        } catch(IOException e) {
            //
        }
    }
}