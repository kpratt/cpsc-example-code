#include <stdio.h>
#include <stdlib.h>
#include "library/array_list.h"

int doubleIt(void* in, void** out){
	int n = *(int*)in;
  int* ret = (int*)malloc(sizeof(int));
	*ret = n * 2;
	*out = ret;
	return 0;
}

int main() {
	size_t i;
	int* n;
	struct array_list nums;
	struct array_list doubled;

	array_list_init(&nums);
	array_list_init(&doubled);
	  
	for(i=1; i<=3; i++){
		n = (int*)malloc(sizeof(int));
		*n = i;
		array_list_add(&nums, n);
	}

	array_list_map(&nums, &doubleIt, &doubled);

	for(i=0; i < doubled.size; i++){
		printf("doubled[%d] == %d\n", i, *(int*)doubled.array[i]);
	}

  return 0;
}
