 
struct list_node {
  struct list_node* prev;
  struct list_node* next;
  void* data;
};

struct list_head {
  struct list_node* head;
  struct list_node* tail;
  size_t size;
};

void linked_list_prepend(const void* data, struct list_head*);
void linked_list_append(const void* data, struct list_head* list);
void linked_list_reverse(struct list_head* list);
int  linked_list_foldr(const struct list_head* list, int (*f)(const void*, void*), void* ret);
int  linked_list_foreach(const struct list_head* list, int (*f)(const void*));
void linked_list_filter(const struct list_head* list, void (*f)(const void*, int*), void (*cpyf)(const void*, void**), struct list_head* out);
void linked_list_free( struct list_head* list );