#include <stdlib.h>

struct node {
  const void* data;
  struct node* next;
};


typedef struct {
  size_t size;
  struct node* head;
  struct node* tail;
  size_t capacity;
} queue_t;

void blocking_queue_init(queue_t* q, size_t capacity);

int blocking_queue_insert(queue_t* q, const void* in);

int blocking_queue_remove(queue_t* q, void** out);
