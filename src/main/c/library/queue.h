#pragma once

/* definition for Queues */
#include <pthread.h>

#define MAX_QUEUE_SIZE 4096

typedef void message_t;

// queue structure
typedef struct{
   int      head; __attribute__((aligned(64)))
   size_t   unclaimedElements;
   message_t *message[MAX_QUEUE_SIZE]; 
   
   int queue_full_waiting;
   int queue_empty_waiting;
   
   //synchronization variables
   pthread_mutex_t      condition_mutex;
   pthread_cond_t       condition_variable_producer;
   pthread_cond_t       condition_variable_consumer;
   
   // flag variable
   int  tail;
   int  size;
}queue_t;


//function declarations
void queue_enqueue( queue_t *queue, const message_t *message );
void queue_dequeue( queue_t *queue, message_t **out );
void queue_init( queue_t *queue );

void message_queue_print_fullempty();

