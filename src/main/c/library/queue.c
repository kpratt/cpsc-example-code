/* Implementation of message queue routines */
#include <stdio.h>
#include <stdlib.h>

#include "queue.h"


// we insert at the tail and increment the index
void queue_enqueue( queue_t *queue, const message_t *message )
{
   pthread_mutex_lock( &(queue->condition_mutex ) );

   if ( queue->unclaimedElements == MAX_QUEUE_SIZE )
   { 
      queue->queue_full_waiting++;
      pthread_cond_wait( &(queue->condition_variable_consumer), &(queue->condition_mutex) ); 
      queue->unclaimedElements--;
   }
   
   // add to the queue
   queue->message[queue->tail] = (message_t*)message;

   // update counters
   queue->tail = (queue->tail+1) & (MAX_QUEUE_SIZE-1);
   queue->size++;
   queue->unclaimedElements++;
    
   // check if the queue had been previously empty
   if( queue->size == 1 ) {
      pthread_cond_signal( &(queue->condition_variable_producer) );   
   }
      
   pthread_mutex_unlock( &(queue->condition_mutex ) ); 
}

/*------------------------------------------------------------------*/
// remove at head and increment the index
void queue_dequeue( queue_t *queue, message_t **out )
{
   pthread_mutex_lock( &(queue->condition_mutex ) );

   // queue empty
   if( queue->size == 0 ) {
      pthread_cond_wait( &(queue->condition_variable_producer), &(queue->condition_mutex) );
   }

   // copy the message from the queue from queue
   *out =  queue->message[queue->head];

   // update counters
   queue->head = (queue->head+1) &(MAX_QUEUE_SIZE-1); 
   queue->size--;

   // if anyones waiting to be signalled
   if( queue->queue_full_waiting > 0 ) {  
       queue->queue_full_waiting--; 
       pthread_cond_signal( &(queue->condition_variable_consumer) );      
       //unclaimed elements will be deceremented by the thread that woke up
   }
   else {
      queue->unclaimedElements--; // baton 
   }
   
   //unlock
   pthread_mutex_unlock( &(queue->condition_mutex ) );
   
}

/*------------------------------------------------------------------*/
void queue_init( queue_t *queue )
{  

   //int n=3;
   //__sync_bool_compare_and_swap (&n, 3, 4);
   // intializinf indexes, signal variables, counters
   queue->head = 0;
   queue->tail = queue->unclaimedElements = queue->size = 0;   

   queue->queue_full_waiting = queue->queue_empty_waiting = 0;

   // intializing synchronization variables   
   pthread_mutex_init( &(queue->condition_mutex), NULL );
   pthread_cond_init ( &(queue->condition_variable_consumer), NULL );
   pthread_cond_init ( &(queue->condition_variable_producer), NULL );  

   // initializing queue
   int index=0;
   for ( index=0; index<MAX_QUEUE_SIZE; index++ )
   {
      queue->message[index] = NULL;
   }  
}