#include <dirent.h>
#include "constants.h"

int for_each_entry(const char* dir, int (*f)(struct dirent*, void*), void*);