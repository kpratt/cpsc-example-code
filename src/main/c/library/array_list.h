#pragma once
#include <stdlib.h>

struct array_list {
  size_t size;
  size_t capacity;
  void** array;
};

void array_list_init(struct array_list* list);
void array_list_add(struct array_list* list, const void* ptr);
void array_list_remove(struct array_list* list, const size_t index, void** ret);
void array_list_size(struct array_list* list, size_t* ret);
int array_list_at(struct array_list* list, const size_t index, void** ret);
void array_list_free(struct array_list* list);
int array_list_map(const struct array_list* list, int (*f)(void*, void**), struct array_list* out);
