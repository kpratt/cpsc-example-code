#include "blocking_queue.h"

void blocking_queue_init(queue_t* q, size_t capacity){
  q->capacity = capacity;
  q->size = 0;
  
  q->head = (struct node*)malloc(sizeof(struct node));
  q->head->next = NULL;
  q->head->data = NULL;
  q->tail = NULL;
}


int blocking_queue_insert(queue_t* q, const void* in){
  size_t s;
  struct node *lkt = q->tail;
  struct node *stepper = lkt;
	struct node* n = (struct node*)malloc(sizeof(struct node));
  n->data = in;
  n->next = NULL;
  do {
    //find the real tail
    while (stepper->next != NULL){
      stepper = stepper->next;
    }
    
    //block if the queue is full
    if(q->size >= q->capacity){
      usleep(1);
      continue;
    }
  } while ( ! __sync_bool_compare_and_swap(&stepper->next, NULL, n) );
  // advance the tail, it needn't be presice only close
  __sync_bool_compare_and_swap(&q->tail, lkt, n);
  // atomic increment
  do {
    s = q->size;
  } while( !__sync_bool_compare_and_swap(&q->size, s, s+1) );
  
  return 0;
}

int blocking_queue_remove(queue_t* q, void** out){
  size_t s;
  struct node* h;
  do{
    h = q->head;
    //block if the queue is empty
    if(h->next == NULL){
      usleep(1);
      continue;
    }
    *out = (void*)h->next->data;
  } while ( ! __sync_bool_compare_and_swap(&q->head, h, h->next) ); // while failure try again
  // atomic decrement
  do {
    s = q->size;
  } while( !__sync_bool_compare_and_swap(&q->size, s, s-1) );
  free(h);
  
  return 0;
}

