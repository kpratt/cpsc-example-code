#include "array_list.h"
#include <string.h>

#define MIN_CAP 8

void array_list_grow(struct array_list* list){
  if(list->capacity == list->size){
    list->array = realloc(list->array, list->capacity * 2);
    list->capacity *= 2;
  }
}

void array_list_shrink(struct array_list* list){
  if(list->capacity > MIN_CAP && list->capacity >= list->size * 4){
    list->array = realloc(list->array, list->capacity / 2);
    list->capacity /= 2;
  }
}

void array_list_init(struct array_list* list){
  list->size = 0;
  list->capacity = MIN_CAP;
  list->array = (void**)malloc(list->capacity * sizeof(void*));
}

void array_list_add(struct array_list* list, const void* ptr){
  if(list->size == list->capacity){
    array_list_grow(list);
  }
  list->array[list->size] = (void*)ptr;
  list->size++;
}

void array_list_remove(struct array_list* list, const size_t index, void** ret){
  size_t i;
  *ret = list->array[index];
  for(i = index; i < list->size - 1; i++){
    list->array[i] = list->array[i+1];
  }
  list->size--;
  
  if(list->capacity >= list->size * 4){
    array_list_shrink(list);
  }
}

int array_list_at(struct array_list* list, const size_t index, void** ret){
  if(index >= list->size){
    return -1;
  } else {
    *ret = list->array[index];
    return 0;
  }
}

void array_list_size(struct array_list* list, size_t* ret){
  *ret = list->size;
}

void array_list_free(struct array_list* list){
  int i;
  for(i=0; i < list->size; i++){
    free(list->array[i]);
  }
	free(list->array);
}

int array_list_map(const struct array_list* list, int (*f)(void*, void**), struct array_list* out){
	size_t i;
	void* ret;
  for(i=0; i < list->size; i++){
		f(list->array[i], &ret);
		array_list_add(out, ret);
	}
  return 0;
}

