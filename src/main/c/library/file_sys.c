#include "file_sys.h"

int for_each_entry(const char * directory_name, int (*f)(struct dirent*, void*), void* ret) {
    struct dirent *entry;
    int err, close_err=0;
    DIR *dir = opendir(directory_name);
    if(dir == nullptr) {
        return -1;
    }

    entry = readdir(dir);
    while (entry != nullptr) {
        err = f(entry, ret);
        if(err != 0){
            goto error;
        }
        entry = readdir(dir);
    }
error:
    close_err = closedir(dir);
    if(err !=0){
      return err;
    } else {
      return close_err;
    }
}