#include "constants.h"
#include "linked_list.h" 

void linked_list_init(struct list_head* list){
  list->head = nullptr;
  list->tail = nullptr;
  list->size = 0;
}

void linked_list_prepend(const void* data, struct list_head* list){
  struct list_node* node = (struct list_node*)malloc(sizeof(struct list_node));
  if(list->head == nullptr){
    list->tail = node;
    node->next = nullptr;
  } else {
    node->next = list->head;
  }
  list->head = node;
  node->prev = nullptr;
  node->data = (void*)data;
  list->size++;
}

void linked_list_append(const void* data, struct list_head* list){
  struct list_node* node = (struct list_node*)malloc(sizeof(struct list_node));
  if(list->head == nullptr){
    list->head = node;
    node->prev = nullptr;
  } else {
    node->prev = list->tail;
    list->tail->next = node;
  }
  list->tail = node;
  node->next = nullptr;
  node->data = (void*)data;
  list->size++;
}

void linked_list_reverse(struct list_head* list) {
	size_t i;
	void* temp;
	struct list_node* front = list->head;
	struct list_node* end = list->head;
	for(i=0; i < (int)(list->size/2); i++){
		temp = front->data;
		front->data = end->data;
		end->data = temp;
    
    front = front->next;
    end = end->prev;
	}
}

// note that this fold works by mutating the output
int linked_list_foldr(const struct list_head* list, int (*f)(const void*, void*), void* ret){
	int err = 0;
	struct list_node* ptr = list->tail;
	while(err == 0 && ptr != nullptr){
		err = f(ptr->data, ret);
		ptr = ptr->prev;
	}
	
	return err;
}

int linked_list_foreach(const struct list_head* list, int (*f)(const void*)){
  int err = 0;
	struct list_node* ptr = list->head;
	while(err == 0 && ptr != nullptr){
		err = f(ptr->data);
		ptr = ptr->next;
	}
  return err;
}

void linked_list_filter(const struct list_head* list, void (*f)(const void*, int*), void (*cpyf)(const void*, void**), struct list_head* out){
  int bool;
  struct list_node* ptr = list->head;
	while(ptr != nullptr){
    f(ptr->data, &bool);
    if(bool){
      void* cpy;
      cpyf(ptr->data, &cpy);
      linked_list_append(cpy, out);
    }
		ptr = ptr->next;
	}
}

void linked_list_free( struct list_head* list ){
	struct list_node* ptr = list->head;
  struct list_node* last;
  list->tail = nullptr;
  list->head = nullptr;
	while(ptr != nullptr){
		last = ptr;
    ptr = ptr->next;
    free(last->data);
    free(last);
	}
}

/*****************fold example code*********************/

int foldable_append(const void* data, void* list){
	struct list_head* _list = (struct list_head*)list;
	linked_list_append(data, _list);
	return 0;
}

void foldr_reverse(struct list_head* list, struct list_head* out) {
	linked_list_foldr(list, &foldable_append, out);
}


