#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main() {

  int pid = fork(); // parent returns child pid
                    // child returns 0
                    
  if(pid == 0){
    printf("I'm the child.\n");
  } else {
    printf("I'm the parent. The child has PID [ %d ]\n", pid);
  }
  printf("Both procs execute this line\n");

  
  if(pid == 0){
    printf("launch ls\n");
    if( execlp("ls","ls", (const char *)NULL ) < 0){
      printf("%s\n", strerror(errno));
    } else {
      printf("never gets executed\n");
    }
  }
  
  return 0;
}
