#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "library/array_list.h"

struct result {
};

struct task {
  int num;
};

void* perform(void* argument) {
  struct task* job = (struct task*)argument;
  struct result* ret = (struct result*)malloc(sizeof(struct result));
  int i;
  for(i=0; i<100; i++){
    printf("%d ", job->num);
    usleep(0);
  }
  return ret;
}

int main() {
  size_t i;
  const size_t size = 5;

  void** output = malloc(sizeof(void*) * size);
  pthread_t* ts = (pthread_t*)malloc(sizeof(pthread_t) * size);
  struct task* jobs = (struct task*)malloc(sizeof(struct task) * size);

  for(i=0; i<size; i++){
    jobs[i].num = i;
  }
  
  for(i=0; i<size; i++){
    pthread_create (&ts[i], NULL, (void *)&perform, (void*)&jobs[i]);
  }
  
  for(i=0; i<size; i++){
    pthread_join(ts[i], &output[i]);
  }
  printf("\ndone\n");
  
  return 0;
}
