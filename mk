rm CMakeCache.txt
rm CMakeFiles/
rm -rf CMakeFiles/
rm Makefile
rm cmake_install.cmake

cmake -DCMAKE_BUILD_TYPE=Debug CMakeLists.txt
make clean
make
